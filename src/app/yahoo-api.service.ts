import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { serviceURLs } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class YahooApiService {

  private baseUrl: string = serviceURLs.yahooApi;
  private validRegions = [
    'US', 'BR', 'AU', 'CA', 'FR', 'DE', 'HK', 'IN', 'IT', 'ES', 'GB', 'SG'
  ];

  constructor(private http: HttpClient) { }

  public getTrendingTickers(): Observable<any> {
    const url = this.baseUrl + '/getTrendingTickers';
    return this.http.get(url);
  }

  public getQuotes(symbol: string, region: string): Observable<any> {
    const url = this.baseUrl + '/getQuotes/' + symbol + '/' + region;
    return this.http.get(url);
  }

  public isValidRegion(region: string): boolean {
    return this.validRegions.includes(region) ? true : false;
  }
}
