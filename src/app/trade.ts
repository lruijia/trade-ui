export class Trade {
  constructor(public tradeId: number,
              public type: string,
              public ticker: string,
              public quantity: number,
              public price: number,
              public state: string) {
  }
}
